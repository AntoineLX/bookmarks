import Vue from 'vue';
import VueRouter from 'vue-router';
Vue.use(VueRouter);

// load components
import LinksList from './pages/LinksList';
import LinksForm from './pages/LinksForm';

// declare routes
const routes = [
    {
        name: 'index',
        path: '/',
        component: LinksList
    },
    {
        name: 'create',
        path: '/create',
        component: LinksForm
    },
    {
        name: 'edit',
        path: '/:id/edit',
        component: LinksForm
    }
];

const router = new VueRouter({
    routes,
});
  
export default router;
  