// Bootstrap the app
require('./bootstrap');

// Vue
import Vue from 'vue';

// Router
import router from './router';

// Validation
import Vuelidate from 'vuelidate'
Vue.use(Vuelidate)

// Start the app
const app = new Vue({
    router
}).$mount('#app')
