<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Link extends Model
{

    protected $casts = [
        'created_at' => 'datetime:Y-m-d H:00',
    ];

    /**
     * Link user relation
     *
     * @return void
     */
    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }
}
